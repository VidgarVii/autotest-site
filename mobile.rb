require 'watir'
require 'webdriver-user-agent'
require 'yaml'

@file = 'data.yml'
driver = Webdriver::UserAgent.driver(browser: :chrome, agent: :iphone, orientation: :landscape)
@browser = Watir::Browser.new driver
@browser.goto 'm.raned.ru'

def auth user
    @browser.span(:class => 'mat-button-wrapper').click
    @browser.link(:class => 'mat-list-item').click
    @browser.text_field(:type => 'text').set get_login user
    @browser.text_field(:type => 'password').set get_psw user
    @browser.button(:class => 'round-button').click
    
    sleep 1
    
    if @browser.url != "https://raned.ru/lk"
        puts "Авторизация не прошла"
        abort
    end
    @browser.div(:class => 'actions').button(:class => 'round-button').flash
    #@browser.button(:class => 'round-button').click
end
def get_login i
    data = open_file 'data'
    return data['users'][i]['login']       
end
def get_psw i
    data = open_file 'data'
    return data['users'][i]['psw']        
end  
def open_file type
    case type
        when 'data'
            return YAML.load File.read @file
        when 'error'
            return YAML.load File.read @errors
        when 'status'
            return YAML.load File.read @status
    end
end     

auth 1

sleep 400