require 'axlsx'
require 'oj'
require 'json'


file = File.open('miss_name.json', 'r').read
data = Oj.load file

Axlsx::Package.new do |p|
    p.workbook.add_worksheet(:name => "Realtor") do |sheet|
    sheet.add_row ["Риэлтор", "Телефон", "Кол-во предложений"]
    
    data.each do |i|  
        i.each { |name, ph|    
            sheet.add_row [name, ph['phone'], ph['total_offer']]
        }
    end 

    end
    p.serialize('realtors.xlsx')
end   

