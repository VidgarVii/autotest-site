require 'mechanize'
require 'watir'
require 'webdriver-user-agent'

url = 'https://krasnodar.cian.ru/cat.php?deal_type=rent&engine_version=2&is_by_homeowner=1&offer_type=flat&region=4820&type=4&wp=1#'

#driver = Webdriver::UserAgent.driver(browser: :chrome, agent: :iphone, orientation: #:landscape)
#@browser = Watir::Browser.new driver  https://krasnodar.cian.ru
#@browser.goto url
long_rent_ad = {
    'address' => nil,
    'bailee' => 'Собственник',
    'floor' => nil,
    'floor_count' => nil,
    'total_area' => nil,
    'kitchen_area' => nil,
    'living_area' => nil,
    'flat_params' => nil,
    'price' => nil,
    'prepayment' => nil,
    'pledge' => nil,
    'type_house' => nil,
    'description' => nil,
    'phone' => nil,
    'agentnocallme' => false,
    'furniture' => nil,
    'washingcar' => nil,
    'refrigerator' => nil,
    'tv' => nil,
    'children' => nil,
    'animale' => nil,
    'condition of the apartment' => nil,
    'balcony' => nil,
    'view from the window' => nil,
    'lift' => nil,
    'toilet' => nil,
    'garbage chute' => nil,
    'havephone' => nil
}
agent = Mechanize.new
page = agent.get url
ads = page.links_with(href: %r{/rent/flat/\d{1,}})
sleep 2
ads.each do |ad|
    resault_ad = ad.click

    #Собираем данные
    long_rent_ad['address'] = resault_ad.at('.a10a3f92e9-geo--1poV5').text.split('На карте')[0]
    long_rent_ad['flat_params'] = resault_ad.at('h1').text.split(',')[0]
    long_rent_ad['total_area'] = resault_ad.at('h1').text.split(',')[1].split(' ')[0]
    long_rent_ad['price'] = resault_ad.at('.a10a3f92e9-price--xQiE6.a10a3f92e9-price--residential--37Rgt').text.split('₽')[0]
    long_rent_ad['phone'] = resault_ad.at('.a10a3f92e9-info--1EqY5').text.split('телефон')[1].split('Написать сообщение')
    x_area = resault_ad.search('.a10a3f92e9-info-text--3GGPV')[1].text.split(' ')[0]
    y_area = long_rent_ad['total_area'].to_i - x_area.to_i

    if x_area.to_i > y_area.to_i
        long_rent_ad['living_area'] = x_area
        long_rent_ad['kitchen_area'] = y_area
    else
        long_rent_ad['living_area'] = y_area
        long_rent_ad['kitchen_area'] = x_area
    end

    array = ['Тип дома', 'Ремонт', 'Вид из окон', 'Совмещённый санузел']
    data = resault_ad.search('.a10a3f92e9-item--3fV-L').children
    data.each_with_index do |single, index|
            case single.text
                when 'Тип дома'
                long_rent_ad['type_house'] = single.next.text    
                when 'Ремонт'
                    long_rent_ad['condition of the apartment'] = single.next.text    
                when 'Вид из окон'
                    long_rent_ad['view from the window'] = single.next.text    
                when 'Совмещённый санузел'
                    long_rent_ad['toilet'] = single.next.text    
            end
    end

    more_info = resault_ad.search('.a10a3f92e9-container--1NJ6M').children
    more_info.each do |single|
        case single.text
            when 'Можно с детьми'
                long_rent_ad['children'] = single.text    
            when 'Можно с животными'
                long_rent_ad['animale'] = single.text    
        end
    end 
    long_rent_ad['description'] = resault_ad.search('.a10a3f92e9-description-text--3SshI').text


    floor_year = resault_ad.search('.a10a3f92e9-info--2dqct').children
    floor_year.each do |single|
        case single.text
            when 'Этаж'
                long_rent_ad['floor'] = single.next.text.split('из')[0]
                long_rent_ad['floor_count'] = single.next.text.split('из')[1]
        end  
    end

    more_info2 = resault_ad.search('.a10a3f92e9-item--1HEPj')
    more_info2.each do |single|
        case single.text
            when 'Балкон'
                long_rent_ad['balcony'] = true
            when 'Стиральная машина'
                long_rent_ad['balcony'] = true
            when 'Мебель в комнатах'
                long_rent_ad['furniture'] = true
            when 'Телевизор'
                long_rent_ad['tv'] = true
            when 'refrigerator'
                long_rent_ad['balcony'] = true    
        end  
    end
    puts long_rent_ad
    sleep 4
end