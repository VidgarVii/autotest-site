require 'watir'
require 'yaml'
require 'date'
require 'http'
require 'nokogiri'
require 'benchmark'
require 'promise'
require 'future'
require_relative 'actions.rb'	

#TODO

# - Компиляция или Рельсы
# wait_while(&:present?) ждем пока есть элемент
# wait_until(&:present?) ждем пока нет элемента
#- Дублируются фото
#

DATA = 'data.yml'
ERROR = 'errors.yml'
STATUS = 'status.yml'
url= ["http://0.0.0.0:8000","http://test.raned.ru/", "https://raned.ru"]
print = YAML.load File.read DATA

class Raned
	def initialize(url, file, error = 'errors.yml', status = 'status.yml')
		@user_signed_in = false
		@f = File.new('logs.txt', 'a')
		@errors = error
		@f.puts ' '
		@f.puts "----------------------------"
		@f.puts Time.now
		
		response = HTTP.head(url).status
			
		if (response.to_i != 200)
			@f.puts "Сайт упал #{response}"
			abort
		end
			
		Watir.logger.ignore :deprecations
		@browser = Watir::Browser.new #:chrome, headless: true       
		@browser.goto url
		@file = file
		@status = status
		page = @browser.image
			
		if page.src == "http://images.raned.ru/50x.png"
			@f.puts "Сайт упал 503"
			abort
		end

		@f.puts "Cтарт #{response}"
	end    
    
	def auth user
		choise = rand(2)
		write_status 'check-0'

		if @browser.div(:class => 'js-btn-modal').present?
			@browser.div(:class => 'js-btn-modal').click
		else    
			write_error 'auth', 0
			abort
		end

		write_ok
		write_status 'check-1'

		if @browser.div(:id => 'js-modal-login').present?
			write_ok
			write_status 'check-2'

			if choise == 1
				@browser.div(:id => 'js-modal-login').text_field(:class => 'js-email').set get_login user
				@browser.text_field(:class => 'js-password').set get_psw user
			else
				@browser.div(:id => 'js-modal-login').text_field(:class => 'js-email').set 'faik@mail.com'
				@browser.text_field(:class => 'js-password').set 'qwerty1'
			end
			time_before_for_auth = Time.now
			@browser.div(:class => 'js-action').click
			sleep 1
			error_check = @browser.text_field(:class => 'js-password').attribute_value('class')

			if error_check.include?('error-registration')
				write_error 'auth', 2
				@browser.link(:href => '#close-modal').click
				auth user
				sleep 1
			end

			sleep 2
			if @browser.div(:id => 'js-modal-login').present?
				write_error 'auth', 1
				abort
			end

			write_ok
			@user_signed_in = true
			write_status 'check-2.1'
			@f.puts ' '
		else
			@f.puts 'Auth modal = FALSE // not found modal'
			abort
		end      

		time_after_for_auth = Time.now
		time_total_for_auth = time_after_for_auth - time_before_for_auth
		write_status 'check-18'

		@f.puts " #{time_total_for_auth.round(2)} секунд"
	end

	def type_ad type=nil
		write_status 'check-3'
		page_time = Benchmark.realtime do
			@browser.div(:class => 'header-wrap').a(:href => '/add/arenda/kvartir/').click
			@browser.form(:id => 'to_second_step').wait_until(&:present?) 
		end
		@f.puts " "
		write_status 'check-19'
		@f.puts " #{page_time.round(2)} секунд"
		
		type_of_action = Proc.new{ |type|	
			ACTIONS[type][0].each { |link| @browser.a(:href => link).click }
			write_status ACTIONS[type][1]
			add_ad ACTIONS[type][2], ACTIONS[type][3], ACTIONS[type][4]
		}
		
    type_of_action[type.to_sym]   
	end
    
 # Заполняем форму
	def add_ad type, data, more_data
		if @browser.form(:id => 'to_second_step').exists? 
			write_ok 
			
			#Сначало проверяем превью на ошибки
			#Проверка Превью на показ не валидных инпутов
			write_status 'check-4'
			@browser.button(:id => 'show_preview').click
			sleep 2                  
			@browser.div(:class => 'fancybox-outer').exists? ? true : write_error('creatAd', 0)                
			#Проверяем кол-во ошибок по дефолту
			li = @browser.div(:class => 'fancybox-outer').ul(:id => 'list_errors').html
			doc = Nokogiri::HTML.parse li                

			count_errrors = case type
											when 'arenda' then 13
											when 'prodazha' then 12
											when 'rental_house' then 10
											when 'sale_house' then 10
											when 'rental_plot_of_land' then 7
											when 'sale_plot_of_land' then 7
											when 'rental_uninhabited_premise' then 12
											when 'sale_uninhabited_premise' then 12
											end
											
			count_li_error = doc.css('li').size
			count_li_error != count_errrors ? write_error('creatAd', 1) : true
			write_ok
			#Закрываем модалку
			write_status 'check-5'
			@browser.button(:id => 'js-close-modal-errors').click
			sleep 2

			if  @browser.button(:id => 'js-close-modal-errors').present?
				write_error 'creatAd', 3   
				abort 
			end
			
			write_ok

			#Сверяем кол-во ошибок с кол-ом подсказок об ошибках
			# .step1_error, .step1_error_select
			write_status 'check-17'
			red_input = @browser.text_fields(class: 'step1_error').size
			red_select = @browser.ps(class: 'step1_error_select').size
			count_red_errors = red_input + red_select
			count_red_errors == count_li_error ? write_ok : write_error('creatAd', 1.1)
			
			#Тестим форму
			#Адрес
			#Открывается ли карта?
			write_status 'check-6'
			@browser.text_field(:id => 'full__address__input').click
			sleep 1
			!@browser.div(:class => 'map__container__geo-point').present? ? write_error('creatAd', 5) : true
			write_ok                
			write_status 'check-7'
			@browser.text_field(:id => 'full__address__input').set(get_address 0)
			sleep 1
			@browser.ul(:id => 'ui-id-1').li.click
			sleep 1
			write_ok
			write_status 'check-8'

			if @browser.span(:class => 'pop_up__geocoder__accept__text').exists? 
				write_ok
				@browser.button(:class => 'close__map').click
			else 
				write_error('creatAd', 4)
			end

			#Заполняем форму
			#Обязательные поля 

			write_status 'check-9'
			@f.puts ' '
			2.times {
				@f.puts "==========================================="
			}

			data.each do |index|						
				@browser.divs(:class => 'SumoSelect')[index].click
				size = @browser.divs(:class => 'SumoSelect')[index].lis.size
				@f.print @browser.divs(:class => 'SumoSelect')[index].p(:class => 'CaptionCont').text
				@f.print " - "

				if type == 'rental_house' && index == 4 
					@browser.divs(:class => 'SumoSelect')[index].lis[2].click
				else
					@browser.divs(:class => 'SumoSelect')[index].lis[rand(1...size)].click
				end

				if @browser.divs(:class => 'SumoSelect')[index].select(:name => 'bailee').exist?
					@bailee_check = @browser.divs(:class => 'SumoSelect')[index].select(:name => 'bailee').text[0]
				end
				
				@f.puts @browser.divs(:class => 'SumoSelect')[index].p(:class => 'CaptionCont').text
			end        

			#Цена
			price_check = rand(2)

			if price_check == 0
				price = rand(1..100000)
				currency = @browser.span(:class => 'small_selecter').text
				@browser.text_field(:id => 'price_for_unit').set price   
				@f.puts "Цена - #{price} #{currency}"
			else
				@browser.label(:for => 'for_negotiated_price_').click
				@f.puts "Цена - Договорная"
			end
			
			#Площадь и этаж
			#Правая колонка
			#Check area 
			proc = Proc.new { |i, name, set|
			@browser.divs(:class => 'right-list__inner')[i].text_field(:name => name).set set
			}

			if type == 'rental-aportment' || type == 'sale-aportment'
				if rand(2) == 1   
					area = rand(30..100)
					if area*0.30 < 15
						kitchen_area = 15
						living_area = (area-kitchen_area-4)
					else
						kitchen_area = area*0.30
						living_area = (area-kitchen_area-10)
					end
					rooms_area = living_area
				else
					area = rand(500)
					kitchen_area = rand(1..500)
					living_area = rand(1..500)
					rooms_area = rand(1..500)
				end

					#Check floor
				if rand(2) == 1
					floor_count = 10
					floor = rand(floor_count)
				else
					floor = 10
					floor_count = rand(floor_count)
				end

				more_info = Hash[[[:area, :living_area, :kitchen_area, :rooms_area, :floor, :floor_count],[area, living_area, kitchen_area, living_area, floor, floor_count]].transpose]

				@f.puts "Общая площадь - #{area}"
				@f.puts "Площадь кухни - #{kitchen_area}"
				@f.puts "Жилая площадь - #{living_area}"
				@f.puts "Этаж 10/10"

				more_info.each { |k, v|
						@browser.divs(:class => 'right-list')[0].text_field(:name => k.to_s).set v.round(2)
						}
			elsif type == 'rental_house'
				proc[0, 'area', rand(50..100)]
				proc[0, 'ground_area', rand(100..250)]
				proc[1,'floor_count', rand(1..4)]
			elsif type == 'rental_plot_of_land'|| type == 'sale_plot_of_land'
				proc[0, 'area', rand(50..300)]
			elsif type == 'rental_uninhabited_premise'
				proc[0, 'area', rand(50..100)]
				proc[0, 'floor_count', 2]                
				proc[0, 'floor', 1]               
				proc[0, 'room_count', rand(1..10)]               

				@browser.divs(:class => 'js-checkbox-group')[1].spans[rand(2)].click
			end
				
		# НАДО БОООЛЬШЕ РЕСУРСОВ!

				@browser.link(:class => 'js-options-btn').click
				sleep 2
				more_data.each_with_index do |num, index|
					@browser.divs(:class => 'SumoSelect')[num].click
					size = @browser.divs(:class => 'SumoSelect')[num].lis.size
					@browser.divs(:class => 'SumoSelect')[num].lis[rand(1...size)].click
						
						# Считываем введенные данные
					some_integer = (type == 'rental-aportment')?1:2												

					@f.print @browser.divs(:class => 'right-list__inner')[some_integer].divs(:class => 'block__params')[index].div(:class => 'text__label').text
					@f.puts @browser.divs(:class => 'SumoSelect')[num].p(:class => 'CaptionCont').text
				end
						
				#Заливаем фото с file_field
				count_image = rand(get_photo.size)

				count_image.times {
						@browser.span(:id => 'select_photos').file_field(:id => 'input_photos').set get_photo[rand(get_photo.size)]
						
				}
				@f.puts "Загружено #{count_image} фото"
				
				description = "Ullamco ut in nulla aliqua nostrud aliquip. Pariatur nisi cupidatat id Lorem Lorem officia est nisi ea nostrud ipsum ullamco sint. Dolor ex ex laborum dolor veniam consequat proident nisi nisi culpa non eiusmod velit aliquip. Et laborum aliquip ipsum qui"
				@browser.textarea(:name => 'description').set description
				
				write_status 'status'
				2.times {
						@f.puts "=============================================="
				}
		else
			write_error 'creatAd', 2
		end
	end

	def add_phone_and_mail
		words = [('a'..'z'), (1..9)].map(&:to_a).flatten
		name = (0...8).map { words[rand(words.length)] }.join
		mail = (0...6).map { words[rand(words.length)] }.join
		domen = (0...2).map { words[rand(words.length)] }.join
		mail = "#{name}@#{mail}.#{domen}"
		@browser.text_field(:name => 'phone'). set '81111111111'
		@browser.text_field(:name => 'email'). set mail

		@browser.button(:class => 'offer_placement').click
		sleep 2
		if @browser.div(:id => 'step_advices').present?
			@browser.button(:class => 'publish_ad').click 
		end
		sleep 50
	end
		
	def check_preview
		@browser.button(:id => 'show_preview').click

		symbol_time = Benchmark.realtime do  
			@browser.div(:class =>'fixed_spinner').wait_while(&:present?)
		end

		puts "Загрузка превью #{symbol_time.round(2)} секунд"
		@f.puts "Загрузка превью #{symbol_time.round(2)} секунд"

		# If chek
		if @browser.div(:class =>'modal-dialog').present? 
			@bailee = @browser.div(:class =>'modal-dialog').link(:class => 'user-mark').text
			write_status 'check-16' 
			(@bailee_check == @bailee)? write_ok : write_error('creatAd', 10)						
			data = @browser.div(:class =>'modal-dialog').text

			data = data.split("\n").reject{ |point|	MISS_WORD.include? point } 
		
			write_status 'check-10' 
			@f.puts ''
			@browser.link(:class => 'close-modal').wait_until_present.click
				
		# If not check
		elsif @browser.div(:class => 'fancybox-outer').div(:id => 'step_errors').present?
			errors = @browser.div(:class => 'fancybox-outer').div(:id => 'step_errors').ul(:id => 'list_errors').lis
			write_status 'check-11'
			@f.puts ''
			
			floor_f = []

			errors.each { |error|
				puts case error.text
							when "Этажей" then "Count floors"
							when "Этаж не может превышать число этажей" then "floor more floors"								
							when "Площадь кухни не может превышать общую площадь" then "Area kitchen"								
							when "Жилая площадь не может превышать общую площадь" then "Living area"								
							end             
			}
			
			@browser.button(:id => 'js-close-modal-errors').click
			sleep 2
			@browser.text_field(:name => 'floor_count').set '11'
			@browser.text_field(:name => 'area').set '700'
		
			sleep 3		
			check_preview

			# If All Bad
		else
			write_error 'creatAd', 6
			abort
		end              
	end
    
	def created_ad        
		if @user_signed_in == true
			write_status 'check-12'
			@browser.button(:class => 'offer_placement').click
			sleep 2
			
			if @browser.div(:id => 'step_advices').present?
				@browser.div(:class => 'step-one-orange').click
				sleep 2

				if !@browser.div(:id => 'step_advices').present?
					@browser.button(:class => 'offer_placement').click
					sleep 2							
				else
					write_error 'creatAd', 7
				end

				write_ok
				@browser.button(:class => 'publish_ad').click 
				id_ad = @browser.div(:id => 'after-publishing-ad').p(:class => 'twelve-size').text.split('№')[1].split('.')[0]
				@f.puts "\nID объявления = #{id_ad}"
				sleep 4

				if @browser.div(:id => 'after-publishing-ad').present?
						write_status 'check-13'
				else
						write_error 'creatAd', 8
				end

				#italic-type twelve-size
				check_time_open_lk = Benchmark.realtime do
					@browser.link(:class => 'my_pubs').click
					@browser.text_field(:class => 'middle').wait_until(&:present?)
				end

				@f.puts ''
				write_status 'check-15'
				@f.puts " #{check_time_open_lk.round(2)} секунд"

				check_lk id_ad
			else
				check_errors :created_ad
			end
		else
			add_phone_and_mail
		end
	end
    
	def check_lk id
		@browser.text_field(:class => 'middle').set id
		@browser.div(:class => 'js-find_ad_by_id').click
		check1 = @browser.divs(:class =>'my-card').size
		sleep 1 
		check2 = @browser.divs(:class =>'my-card').size

		if check1 == check2 && @browser.div(:class => "this_card_#{id}").present?
			write_error 'lk', 0
		else 
			@f.puts ''
			write_status 'check-14'
			write_ok
		end   
	end

	private    

	def check_errors method  
		check_error_list = ['Для размещения объявления необходимо указать корректный номер телефона.','Для размещения объявления необходимо указать ваш email']

		if @browser.div(:id => 'toast-container').wait_until_present(deprecated_timeout=3)
			error_list = @browser.div(:id => 'toast-container').divs(:class => 'toast-message')
			
			error_list.each { |error| 
				if error.text == check_error_list[0]
					@browser.text_field(:name => 'phone'). set '81111111111'
				elsif error.text == check_error_list[1]
					@browser.text_field(:name => 'email'). set 'test777@mail.ru'
				end
			}
		
			@browser.div(:id => 'toast-container').wait_while(&:present?)
			send method   
		else
			write_error 'creatAd', 9
		end
	end
    
	def write_error code, i
		data = open_file 'error'
		@f.puts data[code][i]
		@browser.screenshot.save "screenshots/#{code}_#{i}.png"
	end

	def write_status code
		data = open_file 'status'
		@f.print data['quest'][code]
	end
	
	def write_ok
		data = open_file 'status'
		@f.puts " "+ data['status']
	end

	def get_photo
		data = open_file 'data'
		return data['photo']
	end
    
	def get_login i
		data = open_file 'data'
		return data['users'][i]['login']       
	end  

	def get_psw i
		data = open_file 'data'
		return data['users'][i]['psw']        
	end
	
	def get_address i
		data = open_file 'data'
		return data['address'][i]       
	end

	def open_file type
		case type
		when 'data'
				return YAML.load File.read @file
		when 'error'
				return YAML.load File.read @errors
		when 'status'
				return YAML.load File.read @status
		end
	end 
end

test = []
#30 процессов дохнут( (максимальный успешный тест 15 head и 20 headless)
1.times do 
	test << future {
		#["http://0.0.0.0:8000","http://test.raned.ru/", "https://raned.ru"]
		f = Raned.new url[1], DATA
		
		#учетная запись
		f.auth 0
		
		#Тип объявления
		# arenda, prodazha, rental_house, sale_house, rental_plot_of_land, sale_plot_of_land, rental_uninhabited_premise, sale_uninhabited_premise
		f.type_ad 'rental_house'
		f.check_preview
		f.created_ad
	}
end

p test
