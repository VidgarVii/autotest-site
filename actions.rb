ACTIONS = { prodazha: [['/add/prodazha/kvartir/'], 'check-3.2', 'sale-aportment', [1,5,6], (13..22)],
  arenda: [[], 'check-3.1','rental-aportment', [0,4,5,6], (8..23)],
  rental_house: [['/add/arenda/domov-dach-kottedzhey/'], 'check-3.3', 'rental_house', [0,1,2,3,4], (6..21)],
  sale_house: [['/add/prodazha/kvartir/', '/add/prodazha/domov-dach-kottedzhey/'], 'check-3.6', 'rental_house', [0,1,2,3,4], (6..17)],
  rental_plot_of_land: [['/add/arenda/zemelnih-uchastkov/'], 'check-3.4', 'rental_plot_of_land', [0,2,3], (5..15)],
  sale_plot_of_land: [['/add/prodazha/kvartir/', '/add/prodazha/zemelnih-uchastkov/'], 'check-3.7', 'sale_plot_of_land', [0,2,3], (5..14)],
  rental_uninhabited_premise: [['/add/arenda/kommercheskoy-nedvizhimosti/'], 'check-3.8', 'rental_uninhabited_premise', [0,1,2,3], (5..10)],
  sale_uninhabited_premise: [['/add/prodazha/kvartir/', '/add/prodazha/kommercheskoy-nedvizhimosti/'], 'check-3.9', 'rental_uninhabited_premise', [0,1,2,3], (5..10)]
}
MISS_WORD = ['Доп. сведения', 'Подробнее', '']