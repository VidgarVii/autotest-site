# -*- encoding: utf-8 -*-
# stub: promise 0.3.1 ruby lib

Gem::Specification.new do |s|
  s.name = "promise".freeze
  s.version = "0.3.1"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Ben Lavender".freeze]
  s.date = "2014-12-23"
  s.description = "    A glimpse of a promising future in which Ruby supports delayed execution.\n    Provides global 'promise' and 'future' methods.\n".freeze
  s.email = "blavender@gmail.com".freeze
  s.homepage = "http://promise.rubyforge.org/".freeze
  s.licenses = ["Public Domain".freeze]
  s.required_ruby_version = Gem::Requirement.new(">= 1.8.2".freeze)
  s.rubyforge_project = "promising-future".freeze
  s.rubygems_version = "2.7.7".freeze
  s.summary = "Promises and futures for Ruby".freeze

  s.installed_by_version = "2.7.7" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<rspec>.freeze, [">= 1.3.0"])
      s.add_development_dependency(%q<yard>.freeze, [">= 0.5.8"])
    else
      s.add_dependency(%q<rspec>.freeze, [">= 1.3.0"])
      s.add_dependency(%q<yard>.freeze, [">= 0.5.8"])
    end
  else
    s.add_dependency(%q<rspec>.freeze, [">= 1.3.0"])
    s.add_dependency(%q<yard>.freeze, [">= 0.5.8"])
  end
end
