# -*- encoding: utf-8 -*-
# stub: future 0.0.1 ruby lib

Gem::Specification.new do |s|
  s.name = "future".freeze
  s.version = "0.0.1"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["cho45".freeze]
  s.autorequire = "".freeze
  s.date = "2008-06-14"
  s.description = "async library".freeze
  s.email = "cho45@lowreal.net".freeze
  s.extra_rdoc_files = ["README".freeze, "ChangeLog".freeze]
  s.files = ["ChangeLog".freeze, "README".freeze]
  s.homepage = "http://lowreal.rubyforge.org".freeze
  s.rdoc_options = ["--title".freeze, "future documentation".freeze, "--charset".freeze, "utf-8".freeze, "--opname".freeze, "index.html".freeze, "--line-numbers".freeze, "--main".freeze, "README".freeze, "--inline-source".freeze, "--exclude".freeze, "^(examples|extras)/".freeze]
  s.rubyforge_project = "lowreal".freeze
  s.rubygems_version = "2.7.7".freeze
  s.summary = "async library".freeze

  s.installed_by_version = "2.7.7" if s.respond_to? :installed_by_version
end
